﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public enum DriveUnit
{
    FullWD,
    RWD,
    FWD
}

[Serializable]
public enum Directionof
{
    forward,
    backward,
    neutral
}
public class CarEngineAndController : MonoBehaviour
{

    [Header("Двигатель")]
    public float hRPM; // холостые обороты
    public float xRPM; // максимальные обороты двигателя
    public float mRPM; // минимальные обороты при понижении передачи
    public float eRPM; // переключение передачи при достижении max значение, - повышение передачи
    public float tRPM; // текущая работа двигателя
    public float wRPM = 0;//обороты ведущих колес

    [Header("Привод")]
    public DriveUnit _DriveUnit;
    [Header("Направление")]
    public Directionof _Directionof;

    [Header("Скорость")]
    public float maxSpeed; //максимальная доступная скорость
    public float _speed;//текущая скорость
    public float maxBackSpeed;// скорость задней передачи
    public int backwardForce; // сила на заднюю передачу
    public float motorTorque; // сила дающая нам движение авто, регулируемая с помощью кривой

    [Header("Мощность")]
    public AnimationCurve eCurve;
    public Rigidbody rb;

    [Header("Коробка передач")]
    public float[] gRatio;//передаточные числа
    public int Gear;// текущая передача
    public float mGear;// главная пара

    [Header("Угол поворота колёс")]
    public float steerAngle;
    float steer;// наш инпут horizontal
    [Header("Передние колёса")]
    public WheelCollider FL;
    public WheelCollider FR;
    [Header("Задние колёса")]
    public WheelCollider RL;
    public WheelCollider RR;
    public float gas_button; // инпут verical

    public GameObject COM; // центр массы автомобиля
    public bool isGasandstop;
    public bool isBackward;

    [Header("Хелперы передачи")]
    public bool upRPM;
    public bool downRPM;

    void Start()
    {
        _Directionof = Directionof.neutral;
        rb.centerOfMass = new Vector3(COM.transform.localPosition.x * transform.localScale.x, COM.transform.localPosition.y * transform.localScale.y, COM.transform.localPosition.z * transform.localScale.z);
    }


    void Update()
    {

        gas_button = Input.GetAxis("Vertical");
        steer = Input.GetAxis("Horizontal");

        if (gas_button > 0)
        {
            _Directionof = Directionof.forward;

        }
        else if (gas_button < 0)
        {
            _Directionof = Directionof.backward;
        }
        else
        {
            _Directionof = Directionof.neutral;
        }


        switch (_Directionof)
        {
            case Directionof.forward: isGasandstop = true; isBackward = false; Acceleration(true, false); break;
            case Directionof.backward: isGasandstop = false; isBackward = true; Acceleration(false, true); break;
            case Directionof.neutral: isGasandstop = false; isBackward = false; HolostHod(); break;
        }


    }

    void FixedUpdate()
    {
        //ограничивем скорость, не даём привышать
        if (_speed > maxSpeed)
        {
            rb.velocity = (int)(maxSpeed / 3.6f) * rb.velocity.normalized;
        }

        //обороты колёс, берём передние 2, этого достаточно
        wRPM = (int)(FL.rpm + FR.rpm) / 2;

        if (wRPM == 0)
        {
            _speed = 0;
        }
        else
        {
            _speed = rb.velocity.magnitude * 3.6f;
        }



        //поворот
        FL.steerAngle = steer * steerAngle;
        FR.steerAngle = steer * steerAngle;

    }

    void Acceleration(bool forward, bool backward)
    {

        if (forward)
        {


            if (tRPM > xRPM && Gear != 0)
            {
                tRPM -= 55;
            }
            else if (Gear != (gRatio.Length - 1))
            {
                tRPM += 288 * Time.deltaTime;
            }
            else


            {
                tRPM += 255 * Time.deltaTime;
            }


            if (tRPM > eRPM && Gear < (gRatio.Length - 1))
            {

                Gear++;

                motorTorque = eCurve.Evaluate(tRPM * gRatio[Gear] * mGear);
                Debug.Log(motorTorque);
                tRPM = mRPM;
                WheelTorque(motorTorque);
            }

        }
        if (backward)
        {

        }
    }

    void HolostHod()
    {

        #region Приравниваем обороты / на будущее
        //if (wRPM > 0 && tRPM < mRPM)
        //{
        //    tRPM = wRPM * 100 * Time.deltaTime;
        //} 
        #endregion


        if (tRPM > hRPM)
        {
            tRPM -= 65;
        }
        else
        {
            tRPM += 40;
        }
        //motorTorque = 0;
        //WheelTorque(motorTorque);
    }

    void WheelTorque(float torque)
    {
        switch (_DriveUnit)
        {
            case DriveUnit.FullWD:
                FL.motorTorque = torque;
                FR.motorTorque = torque;
                RR.motorTorque = torque;
                RL.motorTorque = torque;
                break;
            case DriveUnit.FWD:
                FL.motorTorque = torque;
                FR.motorTorque = torque;
                break;
            case DriveUnit.RWD:
                RR.motorTorque = torque;
                RL.motorTorque = torque;
                break;

        }

    }

    void WheelBreaking()
    {

    }

    void WheelBackward()
    {

    }
}